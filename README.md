# Symphony Dev

https://symfony.com/doc/current/the-fast-track/en/index.html

Self-Host Google Fonts
https://google-webfonts-helper.herokuapp.com/fonts

Favicon Generator
https://google-webfonts-helper.herokuapp.com/fonts


## Init project

Composer last version:

composer self-update
composer self-update --2


Creating Symfony Applications:

composer create-project symfony/website-skeleton my_project_name  
composer create-project symfony/skeleton my_project_name


Install other dependencies:

composer require mailgun-mailers


Display information about the project:  
php bin/console about

Test cases (in tests folder):  
php bin/phpunit

The file symfony.lock is the proper lock file for Symfony recipes instead of trying to guess via the state of composer.lock.  
Must be committed to your code repository (CVS).

"php": {  
    "version": "7.2"  
}


## Update Node

If you want to upgrade Node.js from the command line, use the n model within the npm command:

sudo npm cache clean -f  
sudo npm install -g n  
sudo n stable  


## Webpack Encore

First, make sure you install Node.js and also the Yarn package manager.  
Encore generates the Webpack configuration that’s used in your webpack.config.js file.  
Install both the PHP and JavaScript dependencies in your project:

composer require symfony/webpack-encore-bundle  
yarn install

After installing Encore, your app already has one CSS and one JS file, organized into an assets/ directory:

assets/app.js  
assets/styles/app.css

To build the assets ( cal fer-ho per generar public > build ):

Compile assets  
yarn encore dev / npm run dev

Recompile automatically when files change  
*yarn encore dev --watch* / npm run watch

Create production build  
yarn encore production / npm run build

You now have three new files, include these in your base layout file:  
public/build/app.js (holds all the JavaScript for your “app” entry)  
public/build/app.css (holds all the CSS for your “app” entry)  
public/build/runtime.js (a file that helps Webpack do its job)  


### Sass

Instead of using plain CSS you can also use Sass.  
To use Sass, rename the app.css file to app.scss and update the import statement in assets/app.js:

import './styles/app.scss';

Then, tell Encore to enable the Sass pre-processor in webpack.config.js:

.enableSassLoader()

Install sass-loader & sass to use enableSassLoader():

yarn add sass-loader@^10.0.0 sass --dev


### PostCSS

PostCSS is a CSS post-processing tool that can transform your CSS in a lot of cool ways.  
Download postcss-loader and any plugins you want, like autoprefixer:

yarn add postcss-loader autoprefixer path --dev

Next, create a postcss.config.js file at the root of your project:

module.exports = {  
    plugins: {  
        autoprefixer: {}  
    }  
}

Then, enable the loader in webpack.config.js:

.enablePostCssLoader()

Adding browserslist to package.json:

"browserslist": [  
    "defaults"  
]


### Image files

You can use the copyFiles() method in webpack.config.js to copy those files into your final output directory.

.copyFiles({  
    from: './assets/images',  
    to: 'images/[path][name].[ext]',  
})

Install file-loader to use copyFiles():

yarn add file-loader@^6.0.0 --dev


### Asset Versioning

By calling enableVersioning(), each filename will now include a hash that changes whenever the contents of that file change.  
This allows you to use aggressive caching strategies, ignoring any existing cache:

.enableVersioning()

To link to these assets, Encore creates two files entrypoints.json and manifest.json.  
In your app, you need to read this file if you want to be able to link to certain assets.  
just activate the json_manifest_file versioning strategy, config/packages/assets.yaml:

assets:  
    json_manifest_path: '%kernel.project_dir%/public/build/manifest.json'


### Tailwind CSS

A utility-first CSS framework packed with classes. Tailwind includes an expertly crafted set of defaults out-of-the-box, but literally everything can be customized. Tailwind automatically removes all unused CSS when building for production.  
Install Tailwind via npm:

yarn add tailwindcss@latest --dev

For most real-world projects, we recommend installing Tailwind as a PostCSS plugin., add in postcss.config.js:

plugins: {  
    tailwindcss: {},  
    autoprefixer: {},  
}

If you'd like to customize your Tailwind installation, create a minimal tailwind.config.js file at the root of your project:

npx tailwindcss init

When building for production, be sure to configure the purge option to remove any unused classes for the smallest file size in tailwind.config.js:

purge: {  
    enabled: true,  
    content: [  
        './templates/**/*.html.twig',  
    ],  
},

Use the @tailwind directive to inject Tailwind's base, components, and utilities styles in app.scss:

@tailwind base;  
@tailwind components;  
@tailwind utilities;  



## First page

Create a route in the config/routes.yaml file:
home:  
    path: /  
    controller: App\Controller\LuckyController::function


Instead of defining your route in YAML, Symfony also allows you to use annotation routes.  
You can define them in a separate PHP file. --> Remove routes.yaml to use routes.php (Kernel.php)


Show project routes:  
php bin/console debug:router



## Debug toolbar

Create the .htaccess file in /public. !!!


## Create controller

php bin/console make:controller ListController

This will create two new files for you: a controller located in src/Controller/ListController.php and a view page in templates/list/index.html.twig.


## Template engine

Install the twig package:

composer require twig

Twig allows to extend, include, embed,...

Linting and Inspecting Twig Templates:

php bin/console lint:twig  
php bin/console debug:twig

Dump Twig Utilities:

{% dump articles %}  
{{ dump(article) }}


## App global variable

Symfony creates a context object that is injected into every Twig template automatically as a variable called app. It provides access to some application information.


## Translations

Set app supported locales parameters (array/collection in config > sevices.yaml :

app.supported_locales: ['es', 'ca']

https://stackoverflow.com/questions/37098837/twig-links-to-current-route-but-changing-locale  
Idiomes permesos ( config > config.yml > app_locales )

Definir idioma per defecte ( config > packages > translation.yaml ).


https://symfony.com/doc/current/session/locale_sticky_session.html


## Installing Doctrine

Install Doctrine support via the orm Symfony pack:

composer require symfony/orm-pack  
composer require --dev symfony/maker-bundle

Doctrine commands, run to see a full list:

php bin/console list doctrine


# Configuring the Database

The database connection information is stored as an environment variable called DATABASE_URL inside .env:

DATABASE_URL="mysql://root:root@127.0.0.1:8889/symphony-dev?serverVersion=5.7"

When connection parameters are setup, Doctrine can create the db_name database for you:

php bin/console doctrine:database:create


# Creating an Entity Class

Suppose you need a Product object. You can use the make:entity command to create this class and any fields you need.  
The command will ask you some questions:

php bin/console make:entity

When you're ready, create and run a migration with:

php bin/console make:migration  
php bin/console doctrine:migrations:migrate

You can edit the class to add the new property. You can also use make:entity again.  
After the new property is mapped, we have to generate a new migration.  
Each time you make a change to your schema, run these two commands to generate the migration and then execute it.

If you prefer to add new properties manually:

/**  
 * @ORM\Column(type="string", length=255)  
 */  
private $author;

The make:entity command can generate the getter & setter methods for you:

php bin/console make:entity --regenerate  
App\Entity


## Persisting Objects to the Database

It’s time to save a object to the database! Let’s create a new controller:

php bin/console make:controller NewsController

You can query the database directly:

php bin/console doctrine:query:sql 'SELECT * FROM news'


## Validating Objects

The Symfony validator reuses Doctrine metadata to perform some basic validation tasks. This automatic validation is a nice feature to improve your productivity, but it doesn’t replace the validation configuration entirely. You still need to add some validation constraints to ensure that data provided by the user is correct.

Once you’ve fetched an object from Doctrine, you interact with it the same as with any PHP model

https://symfony.com/doc/current/doctrine.html#installing-doctrine


## EasyAdmin

Add EasyAdmin as a project dependency:

composer require admin:^3

EasyAdmin automatically generates an admin area for your application based on specific controllers. Create a a new src/Controller/Admin/ directory where we will store these controllers:

mkdir src/Controller/Admin/

Generate a “web admin dashboard”, will be the main entry point to manage the website data:

php bin/console make:admin:dashboard

Let’s generate a CRUD for model:

php bin/console make:admin:crud

Add link to model in admin menu Controller > Admin > DashboardController > configureMenuItems():

yield MenuItem::linkToCrud('News', 'fas fa-list', News::class);

The default admin backend works well, but it can be customized in many ways to improve the experience.

https://symfony.com/doc/current/the-fast-track/en/9-backend.html


## Securing the Admin Backend

Add security component:

composer require security

We are going to create a fully functional authentication system for the admin.  
We will therefore only have one user, the website admin.  
The first step is to define a User entity:



## FAQS

# How to get config parameters in Symfony Twig Templates?

Defined the config parameter in config > services.yaml like this:

parameters:  
    app.version: 0.1.0

Use parameter substitution in the twig globals section in config > packages > twig.yaml:

twig:  
    globals:  
        version: '%app.version%'  

Then in Twig template:

{{ version }}
