/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';


// require jQuery and create global

const $ = require('jquery');
global.$ = global.jQuery = $;


// other plugins (js + css)

require('slick-carousel');
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


$(document).ready(function() {
    
    console.log('xxx');

    $('.your-class').slick({
        'rows': 0
    });

});
