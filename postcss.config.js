module.exports = {
    plugins: {
        // include whatever plugins you want
        tailwindcss: { config: './tailwind.config.js' },

        // add browserslist config to package.json (see below)
        autoprefixer: {}
    }
}
