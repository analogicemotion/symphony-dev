<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\SeoController;

class DemoController extends AbstractController
{

    /**
     * @Route("/", name="root")
     * @Route("/{_locale}", name="home")
     */
    public function index(): Response
    {
        // get seo
        $page_name = '';
        $seo = SeoController::getSeo($page_name);

        // render twig template
        return $this->render('demo/index.html.twig', [
            'seo' => $seo,
            'controller_name' => 'DemoController',
        ]);
    }


    /**
    * @Route({
    *   "es": "/es/demo",
    *   "ca": "/ca/demo"
    * }, name="demo")
    */
    public function number(): Response
    {
        // get random num
        $number = random_int(0, 100);

        // get seo
        $page_name = '';
        $seo = SeoController::getSeo($page_name);

        // render twig template
        return $this->render('demo/number.html.twig', [
                'seo' => $seo,
            'title' => 'Number - Test Symphony',
            'number' => $number,
        ]);
    }


    /**
    * @Route("/{_locale}/blog", name="blog")
    */
    public function blog(): Response
    {
        // get random num
        $number = random_int(0, 100);

        // get seo
        $page_name = '';
        $seo = SeoController::getSeo($page_name);

        // render twig template
        return $this->render('demo/number.html.twig', [
                'seo' => $seo,
            'title' => 'Number - Test Symphony',
            'number' => $number,
        ]);
    }


    /**
    * @Route("/{_locale}/download", name="download")
    */
    public function download(): Response
    {
        // load the file from the filesystem
        $file_path = 'demo/uploads/pdf-test.pdf';
        $file = new File($file_path);

        // rename the downloaded file
        return $this->file($file, 'custom_name.pdf');
    }


    /**
    * @Route("/{_locale}/form", name="form")
    */
    public function form(): Response
    {
        // get seo
        $page_name = '';
        $seo = SeoController::getSeo($page_name);

        // render twig template
        return $this->render('demo/form.html.twig', [
            'seo' => $seo,
            'title' => 'Form - Test Symphony',
            'form' => '',
        ]);
    }


    /**
    * @Route("/{_locale}/send_form", name="send_form")
    */
    public function send_form(): Response
    {
        // return json
        return $this->json(['username' => 'jane.doe']);
    }
}
