<?php

namespace App\Controller;

use App\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class NewsController extends AbstractController
{
    /**
     * @Route("/{_locale}/blog", name="blog")
     */
    public function index(): Response
    {
        $items = $this->getDoctrine()
            ->getRepository(News::class)
            ->findAll();

        // get seo
        $page_name = '';
        $seo = SeoController::getSeo($page_name);

        // render twig template
        return $this->render('news/index.html.twig', [
            'seo' => $seo,
            'controller_name' => 'NewsController',
            'items' => $items
        ]);
    }


    /**
     * @Route("/{_locale}/blog/{id}", name="blog_item")
     */
    public function show(int $id): Response
    {
        $item = $this->getDoctrine()
            ->getRepository(News::class)
            ->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        // get seo
        $page_name = '';
        $seo = SeoController::getSeo($page_name);

        // or render a template
        return $this->render('news/show.html.twig', [
            'id' => $id,
            'seo' => $seo,
            'item' => $item
        ]);
    }


    /**
     * @Route("/{_locale}/create_news", name="create_news")
     */
    public function create(ValidatorInterface $validator): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $now = new \DateTime();

        $item = new News();
        $item->setTitle('Title');
        $item->setAuthor('Author');
        $item->setDescription('Description');
        $item->setDateAdded($now);
        $item->setDateModified($now);

        // validate item
        $errors = $validator->validate($item);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($item);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Saved new product with id '.$item->getId());
    }


    /**
     * @Route("/{_locale}/news/edit/{id}")
     */
    public function update(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $item = $entityManager->getRepository(News::class)->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $entityManager->remove($item);
        $entityManager->flush();

        return $this->redirectToRoute('product_show', [
            'id' => $item->getId()
        ]);
    }


    /**
     * @Route("/{_locale}/news/delete/{id}")
     */
    public function delete(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $item = $entityManager->getRepository(News::class)->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $item->setName('New product name!');
        $entityManager->flush();

        return $this->redirectToRoute('product_show', [
            'id' => $item->getId()
        ]);
    }
}
